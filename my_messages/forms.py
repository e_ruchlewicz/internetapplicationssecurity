from django import forms

from my_messages.models import *


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['title', 'content']


class MessagePermissionsForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['editors']
