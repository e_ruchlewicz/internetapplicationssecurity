from django.conf.urls import url

from . import views

app_name = 'my_messages'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^messages/$', views.all_messages, name='all_messages'),
    url(r'^messages/add/$', views.add_message, name='add_message'),
    url(r'^messages/delete/(?P<pk>[0-9]+)/$', views.delete_message, name='delete_message'),
    url(r'^messages/edit/(?P<pk>[0-9]+)/$', views.edit_message, name='edit_message'),
    url(r'^messages/(?P<pk>[0-9]+)/editors/$', views.message_editors, name='message_editors'),
]
