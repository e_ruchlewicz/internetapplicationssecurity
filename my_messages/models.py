from datetime import datetime

from django.contrib.auth.models import User
from django.db import models


class Message(models.Model):
    title = models.CharField("Title", max_length=50, null=False, blank=False)
    content = models.TextField("Content", max_length=200, null=False, blank=False)
    date = models.DateTimeField("Date", default=datetime.now, null=True, blank=True)
    user = models.ForeignKey(User, related_name="Creator", null=True, blank=True, on_delete=models.SET_NULL)
    editors = models.ManyToManyField(User, related_name="Editors", blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"
