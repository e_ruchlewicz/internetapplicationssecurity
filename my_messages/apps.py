from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = 'my_messages'
