import logging

from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils import timezone

from .forms import MessageForm, MessagePermissionsForm
from .models import Message

now = timezone.now()
logger = logging.getLogger(__name__)


@login_required
def index(self):
    return redirect('my_messages:all_messages')


@login_required
def all_messages(request):
    messages = Message.objects.all()
    return render(request, 'my_messages/all_messages.html', {'messages': messages, 'user': request.user})


def login_view(request):
    form_class = AuthenticationForm
    template_name = 'my_messages/login.html'

    if request.method == 'POST':
        form = form_class(data=request.POST)
        form.fields['password'].label = "Password"
        form.fields['username'].label = "Username"
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                login(request, user)
                return redirect('my_messages:index')
    else:
        form = form_class()
        form.fields['password'].label = "Password"
        form.fields['username'].label = "Username"
    return render(request, template_name, {'form': form})


@login_required
def logout_view(request):
    logout(request)
    return redirect('my_messages:index')


@login_required
def add_message(request):
    form_class = MessageForm
    template_name = 'my_messages/add_message.html'
    form = form_class(request.POST or None)
    if request.POST and form.is_valid():
        message = form.save(commit=False)
        message.user = request.user
        message.save()
        return redirect('my_messages:all_messages')
    return render(request, template_name, {'form': form})


@login_required
def delete_message(**kwargs):
    query = Message.objects.get(id=kwargs['pk']).first()
    query.delete()
    return redirect('my_messages:all_messages')


@login_required
def message_editors(request, **kwargs):
    form_class = MessagePermissionsForm
    template_name = 'my_messages/message_permissions.html'
    message = Message.objects.filter(id=kwargs['pk']).first()
    form = form_class(request.POST or None, instance=message)
    form.fields["editors"].queryset = User.objects.exclude(id=message.user.id)
    print(User.objects.exclude(id=message.user.id))
    if request.POST and form.is_valid():
        message = form.save(commit=False)
        editors = form.cleaned_data['editors']
        message.editors.set(editors)
        message.save()
        return redirect('my_messages:all_messages')
    return render(request, template_name, {'form': form})


@login_required
def edit_message(request, **kwargs):
    form_class = MessageForm
    template_name = 'my_messages/add_message.html'
    message = Message.objects.filter(id=kwargs['pk']).first()
    form = form_class(request.POST or None, instance=message)
    if request.POST and form.is_valid():
        message = form.save(commit=False)
        message.save()
        return redirect('my_messages:all_messages')
    return render(request, template_name, {'form': form})
