from django.contrib import admin

from .models import *


class MessageAdmin(admin.ModelAdmin):
    ordering = ('-date',)
    list_display = ('title', 'date', 'user', 'content')
    list_filter = ('user', 'date')
    search_fields = ['title', 'user__username', 'date']


admin.site.register(Message, MessageAdmin)
